# Prerequisites
```

pip install -r requirements.txt

git clone https://gitlab.com/brandon-flores/food-recognition.git
cd food-recognition


```

### Dataset

Add tf_files folder inside the food-recognition directory
Dataset (tf_files folder as well as the images) can be downloaded in the following links:
* [Dropbox](https://www.dropbox.com/s/maabv9n0op5pc1g/tf_files.zip?dl=0 "https://www.dropbox.com/s/maabv9n0op5pc1g/tf_files.zip?dl=0")
* [Google Drive](https://drive.google.com/open?id=1IHvvEUPoHIJfBPuGh6Xz62Ur_6_tRDVG "https://drive.google.com/open?id=1IHvvEUPoHIJfBPuGh6Xz62Ur_6_tRDVG")

### To create own labels

> Create a folder insider tf_files folder and name it as the general name of the labels (e.g. foods, gender, plants)
> Inside the created folder, create subfolders for each label (e.g. male, female ---> for gender), and place the images there

### Add to shell (bash,zsh,etc.)
```

IMAGE_SIZE=224
ARCHITECTURE="mobilenet_0.50_${IMAGE_SIZE}"


```

### Training
```

python -m scripts.retrain \
  --bottleneck_dir=tf_files/bottlenecks \
  --how_many_training_steps=500 \
  --model_dir=tf_files/models/ \
  --summaries_dir=tf_files/training_summaries/"${ARCHITECTURE}" \
  --output_graph=tf_files/retrained_graph.pb \
  --output_labels=tf_files/retrained_labels.txt \
  --architecture="${ARCHITECTURE}" \
  --image_dir=tf_files/foods


```

### Usage
```

python -m scripts.label_image \
    --graph=tf_files/retrained_graph.pb  \
    --image=for_test/<filename.ext>
    

```

## Members
* Bedio, Aiden Justin
* Flores, Brandon Jay
* Zauleck, Florence Athena
